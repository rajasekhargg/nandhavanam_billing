@ echo off
cd ..
set BASE=private
set POI_CLASSES=private\3rdparty\poi-3.13\poi-3.13-20150929.jar;private\3rdparty\poi-3.13\poi-ooxml-3.13-20150929.jar;private\3rdparty\poi-3.13\ooxml-lib\xmlbeans-2.6.0.jar;private\3rdparty\poi-3.13\poi-ooxml-schemas-3.13-20150929.jar
set JASPER_LIB=private\3rdparty\jasperreports-6.2.0\lib
set JASPER_CLASSES=private\3rdparty\jasperreports-6.2.0\dist\jasperreports-6.2.0.jar;%JASPER_LIB%\commons-logging-1.1.1.jar;%JASPER_LIB%\groovy-all-2.4.3.jar;%JASPER_LIB%\commons-collections-3.2.1.jar;%JASPER_LIB%\commons-digester-2.1.jar;%JASPER_LIB%\itext-2.1.7.js4.jar
set PDF_MERGE=private\3rdparty\pdfbox-app-2.0.0-RC2.jar
set JRE_HOME=private\jre7\bin
set JAVA_HOME="C:\Program Files\Java\jdk1.7.0_79"
set CLASS_PATH=private\classes;%POI_CLASSES%;%JASPER_CLASSES%;%PDF_MERGE%
%JAVA_HOME%\bin\javac -d private\classes -classpath %CLASS_PATH% private\src\com\enigma\billing\*.java
rem %JRE_HOME%\java -classpath %CLASS_PATH% com.enigma.billing.ReportGenerator
cd bin
start ..\output
@ echo on
